from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from .models import Project
from django.contrib.auth.mixins import LoginRequiredMixin


# CHANGE TO CLASS BASED VIEW TOO COMPLICATED
class ProjectListView(LoginRequiredMixin, ListView):
    model = Project
    template_name = "main.html"
    context_object_name = "list_projects"

    def get_queryset(self):
        user = self.request.user
        return Project.objects.filter(members=user)


class ProjectDetailView(LoginRequiredMixin, DetailView):
    model = Project
    template_name = "detail.html"
    fields = "Name", "Assignee", "Start Date", "Due Date", "Is Completed"


class ProjectCreateView(LoginRequiredMixin, CreateView):
    model = Project
    template_name = "create.html"
    fields = "name", "description", "members"

    def get_success_url(self):
        return reverse_lazy("show_project", args=[self.object.id])
